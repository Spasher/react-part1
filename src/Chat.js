import React from "react";
import ProtoTypes from "prop-types";
import Header from "./Header"
import MessageList from "./MessageList"
import MessageInput from "./MessageInput"
import { v4 as uuidv4 } from 'uuid';
import Preloader from "./Preloader";
import './styles/Chat.css'

import moment from "moment";



class Chat extends React.Component{
    constructor(props) {
        super(props);
        this.state ={
            users:4,
            messagesCount:0,
            lastMessage:'',
            isFetching:true,
            ownMessages:[],
            messages:[]
        }
        const res = fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
            .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
            .then((result) => this.onUpdate(result, false, result.length))



        this.messageSend = this.messageSend.bind(this)
    }
    onUpdate(messages,isFetching,messagesCount){
        this.setState({

            messages:messages,
            isFetching:isFetching,
            messagesCount:messagesCount
        })
    }

    onEdit(){

    }
    onDelete(){
        const ownMessages = this.state.ownMessages

        ownMessages.filter((item)=> item.id !== this.props.id)

    }

    messageSend(message) {
        let messageCount = this.state.messagesCount
        messageCount += 1
        const ownMessages = this.state.ownMessages

        const ownMessage = {
            id:uuidv4(),
            text:message.target.value
        }
        ownMessages.push(ownMessage)
        const now = moment().format('LLL');

        this.setState({
            messagesCount:messageCount,
            lastMessage:now,
            ownMessages:ownMessages
        })




    }

    render() {
        const data = this.state

        return (
            <>
            { this.state.isFetching ? <Preloader/>:
            <div className="chat">
                <Header
                    users ={data.users}
                    messagesCount = {data.messagesCount}
                    lastMessage = {data.lastMessage}
                />
                <MessageList
                    messages = {data.messages}
                    ownMessages = {data.ownMessages}
                    onEdit = {this.onEdit}
                    onDelete = {this.onDelete}
                />
                <MessageInput messageSend = {this.messageSend}/>
            </div>
            }
            </>
        )
    }
}

export default Chat