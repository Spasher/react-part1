import React from "react";
import ProtoTypes from "prop-types";

import './styles/Message.css'

class Message extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        const props = this.props
        return (
            <div className="message">
                <div className="message-user-avatar">
                    <img src={props.avatar} alt="UserAvatar"/>
                </div>
                <div className="message__content">
                    <div className="message-user-name">
                        {props.user}
                    </div>
                    <div className="message-text">
                        <p> {props.text}</p>
                    </div>

                    <div className="message-time">
                        {props.createdAt}
                    </div>
                </div>
                <div className="message-like">
                    <svg stroke="red" fill="#fff" width="15" height="15"
                         className="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22">
                        <path  d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/>
                    </svg>

                </div>
            </div>
        )
    }
}
export default Message