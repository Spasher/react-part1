import React from "react";
import ProtoTypes from "prop-types";

import './styles/Header.css'
class Header extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
          <div className="header">
              <div className="header-content">
                  <div className="header-title">My chat</div>
                  <div className="header-users-count">{this.props.users} users</div>
                  <div className="header-messages-count">{this.props.messagesCount} messages</div>
              </div>
              <div className="header-last-message-date">last message{this.props.lastMessage}</div>
          </div>
        )
    }
}

export default Header