import React from "react";
import ProtoTypes from "prop-types";
import Message from "./Message";
import moment from "moment";


import './styles/OwnMessage.css'

class OwnMessage extends React.Component{
    constructor(props) {
        super(props);
    }


    render() {
        const now = moment().format('LLL');
        return (
            <div className="owm-message">
                <div className="message-content">
                    <div className="message-text">
                        {this.props.text}
                    </div>
                    <div className="message-edit">
                        <button onClick={this.props.onEdit.bind(this)}>Edit</button>
                    </div>

                    <div className="message-delete">
                        <button onClick={this.props.onDelete}>Delete</button>
                    </div>
                    <div className="message-time">
                        {now}
                    </div>
                </div>
            </div>
        )
    }
}
export default OwnMessage