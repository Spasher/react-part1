import React from "react";
import ProtoTypes from "prop-types";
import preloader from "./preloader.svg"



import './styles/Preloader.css'

class Preloader extends React.Component{


    render() {
        return(
            <div className="preloader">
                <img  src={preloader} alt=""/>
            </div>
            )

    }
}
export default Preloader