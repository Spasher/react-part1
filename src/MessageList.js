import React from "react";
import ProtoTypes from "prop-types";
import Message from "./Message"
import OwnMessage from "./OwnMessage"
import moment from "moment";

import './styles/MessageList.css'

class MessageList extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        const messages = this.props.messages.sort(function (a,b){
            var c = new Date(a.createdAt);
            var d = new Date(b.createdAt);
            return c-d;
        })
        const ownMessages = this.props.ownMessages
        const children = []

        for(let i= 0; i<messages.length;i++){
            const date = new Date(messages[i].createdAt)
            children.push(
                <Message
                name = {messages[i].user}
                avatar = {messages[i].avatar}
                userId = {messages[i].userId}
                createdAt = {moment(date).format('hh:mm')}
                text = {messages[i].text}
                key = {messages[i].id}
                />)
        }
        for(let i = 0; i<ownMessages.length;i++){
            children.push(
                <OwnMessage
                    key = {ownMessages[i].id}
                    id = {ownMessages[i].id}
                    text = {ownMessages[i].text}
                    onEdit = {this.props.onEdit}
                    onDelete = {this.props.onDelete}

                />
            )
        }
        return (
            <div className="message-list">
                {children}
            </div>
        )
    }
}

export default MessageList