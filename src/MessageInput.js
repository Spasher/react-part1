import React from "react";
import ProtoTypes from "prop-types";

import './styles/MessageInput.css'
class MessageInput extends React.Component{
    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div className="message-input">
                <form >
                    <input type="text" className="message-input-text"/>
                    <button onClick={ this.props.messageSend} type="submit" className="message-input-button">Send</button>
                </form>
            </div>
        )
    }
}
export default MessageInput